const request=require('request');
var rp = require('request-promise');

const cheerio=require('cheerio');
const fs = require('fs');


const fictionalBookstoreScrape='http://books.toscrape.com/?';
var bookTitle; //I want to get the booktitle from the fictive book-page books toscrape
//EMpty array for storing the pages
var pages=[];

//Since the first page have a different URL, this needs to be added manually, the 50 left is added with a for loop
pages[0]=fictionalBookstoreScrape;
for(let i=1; i<=50;i++){
    pages[i]=`http://books.toscrape.com/catalogue/page-${i}.html`;

}

var titleList=[];
    request(fictionalBookstoreScrape, (error,respone,html)=>{
        if(!error &&respone.statusCode==200){
            const $=cheerio.load(html);
            
            /*Finding the title, price and availability on all off each of the books*/ 
            $('ol > li').each((i,el)=>{
                titleList[i]={};
                const title=$(el).find('h3').text();
                const price=$(el).find('.product_price').children('.price_color').text();
                const availability=$(el).find('.product_price').children('.instock').text().replace(/\s\s+/g,''); //The /g is used for global match, elimintating all the spaces
                titleList[i]['Title']=title;
                titleList[i]['Price']=price;
                titleList[i]['Availability']=availability;  
            });
            writeFileToJSON(titleList);
        }
    });


function writeFileToJSON(listToWriteToJSON){
    fs.writeFile("output.json",JSON.stringify(titleList,null),function(err){
        if(err){
            console.log('something went wrong with writing json');
        }
    })
}