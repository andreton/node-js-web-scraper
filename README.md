# In this project I am gonna create an application for web scraping

## Originially my plan was to scrape all 50 pages of the fictional book store, but I get some errors with the synchronisation. I got some input that suggested using request-promise along with await and async. 

Due to lack of time and a new Vue assignment coming up I gave up on the project on scraping all pages and insted scraped the first page. 

Will have to visit this again when I get the time, I need some more knowledge on the event loop and synchronisation in javascript first :) 